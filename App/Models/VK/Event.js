export const Event = {
    CONFIRMATION: 'confirmation',
    MESSAGE_NEW: 'message_new',
    MESSAGE_EVENT: 'message_event'
}