/**
 * @property {String} type
 * @property {Object} object
 * @property {Number} group_id
 * @property {String} secret
 */
export default class VKRequest {
    /** @param {Request} request */
    constructor(request) {
        Object.entries(request.body).forEach(([prop, value]) => {
            this[prop] = value;
        })
    }
}