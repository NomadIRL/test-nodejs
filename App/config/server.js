import _ from 'express';

export const app = _();

const port = 80;

app.listen(port, () => {
    console.log(`server started on port ${port} ` + new Date().getMinutes() + ' min.');
});