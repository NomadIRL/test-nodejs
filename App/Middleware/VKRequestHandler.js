import EventController from "../Controllers/VK/EventController.js";
import VKRequest from "../Models/VK/VKRequest.js";
import {VK_GROUP_ID} from "../config/vk.js";
import {VK_SECRET_KEY} from "../config/vk.js";

export default class VKRequestHandler {
    /**
     * @param {Request} request
     * @param {Response} response
     */
    constructor(request, response) {
        let vkRequest = new VKRequest(request);

        if (vkRequest.group_id !== VK_GROUP_ID) {
            throw new Error('Group id does not match');
        }

        if (vkRequest.secret !== VK_SECRET_KEY) {
            throw new Error('Provided secret is invalid');
        }

        EventController.handle(vkRequest).then(answer => response.send(answer));
    }
}